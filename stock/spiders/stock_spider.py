import logging

import scrapy

from stock.spiders.stock_db import DB


class MySpider(scrapy.Spider):
    name = "stock"

    def __init__(self):
        self.db = DB()

    def start_requests(self):
        # urls = ["https://hq.gucheng.com/gpdmylb.html"]
        #
        # for url in urls:
        #     yield scrapy.Request(url, self.parse)
        return [scrapy.Request("https://hq.gucheng.com/gpdmylb.html", self.parse)]

    def parse(self, response):
        code_names = response.xpath('//*[@id="stock_index_right"]/div[3]/section/a/text()').getall()

        logging.info("stock_code 更新完成")
        self.db.download_stock_csv(code_names)
        self.db.read_csv_dir()
        pass
